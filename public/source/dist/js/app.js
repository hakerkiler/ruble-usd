//
// Here is how to define your module
// has dependent on mobile-angular-ui
//
var app = angular.module('MobileAngularUiExamples', [
    'ngRoute',
    'mobile-angular-ui',

    // touch/drag feature: this is from 'mobile-angular-ui.gestures.js'
    // it is at a very beginning stage, so please be careful if you like to use
    // in production. This is intended to provide a flexible, integrated and and
    // easy to use alternative to other 3rd party libs like hammer.js, with the
    // final pourpose to integrate gestures into default ui interactions like
    // opening sidebars, turning switches on/off ..
    'mobile-angular-ui.gestures'
]);

app.run(function($transform) {
    window.$transform = $transform;
});

//
// You can configure ngRoute as always, but to take advantage of SharedState location
// feature (i.e. close sidebar on backbutton) you should setup 'reloadOnSearch: false'
// in order to avoid unwanted routing.
//
app.config(function($routeProvider) {
    $routeProvider.when('/',
        {
            templateUrl: 'templateApp/home.html',
            reloadOnSearch: false
        }
    );
    $routeProvider.when('/access-settings',
        {
            templateUrl: 'templateApp/access-settings.html',
            reloadOnSearch: false

        }
    );
    $routeProvider.when('/deposit',         {templateUrl: 'templateApp/deposit.html', reloadOnSearch: false});
    $routeProvider.when('/withdrawal',            {templateUrl: 'templateApp/withdrawal.html', reloadOnSearch: false});
    $routeProvider.when('/accordion',       {templateUrl: 'templateApp/accordion.html', reloadOnSearch: false});
    $routeProvider.when('/overlay',         {templateUrl: 'templateApp/overlay.html', reloadOnSearch: false});
    $routeProvider.when('/forms',           {templateUrl: 'templateApp/forms.html', reloadOnSearch: false});
    $routeProvider.when('/dropdown',        {templateUrl: 'templateApp/dropdown.html', reloadOnSearch: false});
    $routeProvider.when('/touch',           {templateUrl: 'templateApp/touch.html', reloadOnSearch: false});
    $routeProvider.when('/swipe',           {templateUrl: 'templateApp/swipe.html', reloadOnSearch: false});
    $routeProvider.when('/drag',            {templateUrl: 'templateApp/drag.html', reloadOnSearch: false});
    $routeProvider.when('/drag2',           {templateUrl: 'templateApp/drag2.html', reloadOnSearch: false});
    $routeProvider.when('/carousel',        {templateUrl: 'templateApp/carousel.html', reloadOnSearch: false});
});

//
// `$touch example`
//

app.directive('toucharea', ['$touch', function($touch){
    // Runs during compile
    return {
        restrict: 'C',
        link: function($scope, elem) {
            $scope.touch = null;
            $touch.bind(elem, {
                start: function(touch) {
                    $scope.touch = touch;
                    $scope.$apply();
                },

                cancel: function(touch) {
                    $scope.touch = touch;
                    $scope.$apply();
                },

                move: function(touch) {
                    $scope.touch = touch;
                    $scope.$apply();
                },

                end: function(touch) {
                    $scope.touch = touch;
                    $scope.$apply();
                }
            });
        }
    };
}]);

//
// `$drag` example: drag to dismiss
//
app.directive('dragToDismiss', function($drag, $parse, $timeout){
    return {
        restrict: 'A',
        compile: function(elem, attrs) {
            var dismissFn = $parse(attrs.dragToDismiss);
            return function(scope, elem){
                var dismiss = false;

                $drag.bind(elem, {
                    transform: $drag.TRANSLATE_RIGHT,
                    move: function(drag) {
                        if( drag.distanceX >= drag.rect.width / 4) {
                            dismiss = true;
                            elem.addClass('dismiss');
                        } else {
                            dismiss = false;
                            elem.removeClass('dismiss');
                        }
                    },
                    cancel: function(){
                        elem.removeClass('dismiss');
                    },
                    end: function(drag) {
                        if (dismiss) {
                            elem.addClass('dismitted');
                            $timeout(function() {
                                scope.$apply(function() {
                                    dismissFn(scope);
                                });
                            }, 300);
                        } else {
                            drag.reset();
                        }
                    }
                });
            };
        }
    };
});

//
// Another `$drag` usage example: this is how you could create
// a touch enabled "deck of cards" carousel. See `carousel.html` for markup.
//
app.directive('carousel', function(){
    return {
        restrict: 'C',
        scope: {},
        controller: function() {
            this.itemCount = 0;
            this.activeItem = null;

            this.addItem = function(){
                var newId = this.itemCount++;
                this.activeItem = this.itemCount === 1 ? newId : this.activeItem;
                return newId;
            };

            this.next = function(){
                this.activeItem = this.activeItem || 0;
                this.activeItem = this.activeItem === this.itemCount - 1 ? 0 : this.activeItem + 1;
            };

            this.prev = function(){
                this.activeItem = this.activeItem || 0;
                this.activeItem = this.activeItem === 0 ? this.itemCount - 1 : this.activeItem - 1;
            };
        }
    };
});

app.directive('carouselItem', function($drag) {
    return {
        restrict: 'C',
        require: '^carousel',
        scope: {},
        transclude: true,
        template: '<div class="item"><div ng-transclude></div></div>',
        link: function(scope, elem, attrs, carousel) {
            scope.carousel = carousel;
            var id = carousel.addItem();

            var zIndex = function(){
                var res = 0;
                if (id === carousel.activeItem){
                    res = 2000;
                } else if (carousel.activeItem < id) {
                    res = 2000 - (id - carousel.activeItem);
                } else {
                    res = 2000 - (carousel.itemCount - 1 - carousel.activeItem + id);
                }
                return res;
            };

            scope.$watch(function(){
                return carousel.activeItem;
            }, function(){
                elem[0].style.zIndex = zIndex();
            });

            $drag.bind(elem, {
                //
                // This is an example of custom transform function
                //
                transform: function(element, transform, touch) {
                    //
                    // use translate both as basis for the new transform:
                    //
                    var t = $drag.TRANSLATE_BOTH(element, transform, touch);

                    //
                    // Add rotation:
                    //
                    var Dx    = touch.distanceX,
                        t0    = touch.startTransform,
                        sign  = Dx < 0 ? -1 : 1,
                        angle = sign * Math.min( ( Math.abs(Dx) / 700 ) * 30 , 30 );

                    t.rotateZ = angle + (Math.round(t0.rotateZ));

                    return t;
                },
                move: function(drag){
                    if(Math.abs(drag.distanceX) >= drag.rect.width / 4) {
                        elem.addClass('dismiss');
                    } else {
                        elem.removeClass('dismiss');
                    }
                },
                cancel: function(){
                    elem.removeClass('dismiss');
                },
                end: function(drag) {
                    elem.removeClass('dismiss');
                    if(Math.abs(drag.distanceX) >= drag.rect.width / 4) {
                        scope.$apply(function() {
                            carousel.next();
                        });
                    }
                    drag.reset();
                }
            });
        }
    };
});

app.directive('dragMe', ['$drag', function($drag){
    return {
        controller: function($scope, $element) {
            $drag.bind($element,
                {
                    //
                    // Here you can see how to limit movement
                    // to an element
                    //
                    transform: $drag.TRANSLATE_INSIDE($element.parent()),
                    end: function(drag) {
                        // go back to initial position
                        drag.reset();
                    }
                },
                { // release touch when movement is outside bounduaries
                    sensitiveArea: $element.parent()
                }
            );
        }
    };
}]);

//
// For this trivial demo we have just a unique MainController
// for everything
//
app.controller('MainController', function($rootScope, $scope, $http, $location){

    $scope.swiped = function(direction) {
        alert('Swiped ' + direction);
    };
    $scope.beackUrl = '';
    $scope.auth = false;
    // User agent displayed in home page
    $scope.userAgent = navigator.userAgent;

    // Needed for the loading screen
    $rootScope.$on('$routeChangeStart', function(e, current, pre){
        $rootScope.loading = true;
        if(typeof pre != 'undefined'){
            $scope.beackUrl = pre.$$route.originalPath;
        }
        // console.log(pre);
        var arr_route = ['/access-settings', '/deposit', '/withdrawal'];
        // (list.indexOf(createItem.artNr) !== -1)
        if(arr_route.indexOf(current.$$route.originalPath) !== -1){
            $scope.menuButton = false;
            $scope.arrowBack = true;
        } else {
            $scope.menuButton = true;
            $scope.arrowBack = false;
        }
    });

    $scope.go = function ( path ) {
        $location.path( path );
    };

    $rootScope.$on('$routeChangeSuccess', function(){
        $rootScope.loading = false;
        $http.get('/check-auth').success(function(data){
            if(data.auth == true){
                $scope.auth = true;
            }
        }).error(function(data){
            console.log('auth = false');
        });
    });

    $scope.back = function() {
        if($scope.beackUrl == ''){
            $location.path('/');
        } else {
            $location.path($scope.beackUrl);
        }
    };

    $scope.loginForm = true;
    $scope.registerForm = false;
    $scope.arrowBack = false;
    $scope.menuButton = true;
    // $scope.email = 'me@example.com';

    $scope.invalidEmail = false;
    $scope.invalidPassword = false;

    $scope.login = {};
    $scope.reg = {};

    $scope.loginSend = function() {
        console.log($scope.login);
        $http.post('/login', $scope.login).success(function(data){
            window.location.reload();
            return false;
        }).error(function(data){
            $.each(data, function(k, v){
                if(k == 'email'){
                    $scope.invalidEmail = true;
                    $scope.invalidEmailMsg = v[0];
                    console.log(v[0]);
                }
                if(k == 'password'){
                    $scope.invalidPassword = true;
                    $scope.invalidPasswordMsg = v[0];
                }
            })
        });
        return false;
    };
    $scope.regFormSend = function() {
        console.log($scope.login);
        $http.post('/register', $scope.reg).success(function(data){
            window.location.reload();
            return false;
        }).error(function(data){
            $.each(data, function(k, v){
                if(k == 'email'){
                    $scope.invalidEmailReg = true;
                    $scope.invalidEmailMsgReg = v[0];
                    console.log(v[0]);
                }
                if(k == 'password'){
                    $scope.invalidPasswordReg = true;
                    $scope.invalidPasswordMsgReg = v[0];
                }
            })
        });
        return false;
    };
    $scope.loginShow = function() {
        $scope.loginForm = true;
        $scope.registerForm = false;
    };
    $scope.regFormShow = function() {
        $scope.loginForm = false;
        $scope.registerForm = true;
    };


    //
    // 'Drag' screen
    //
    $scope.notices = [];

    for (var j = 0; j < 10; j++) {
        $scope.notices.push({icon: 'envelope', message: 'Notice ' + (j + 1) });
    }

    $scope.deleteNotice = function(notice) {
        var index = $scope.notices.indexOf(notice);
        if (index > -1) {
            $scope.notices.splice(index, 1);
        }
    };
});