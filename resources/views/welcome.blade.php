<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <base href="/" />
        <title>Mobile Angular UI Demo</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimal-ui" />
        <meta name="apple-mobile-web-app-status-bar-style" content="yes" />
        <link rel="shortcut icon" href="/favicon.png" type="image/x-icon" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
        <link rel="stylesheet" href="{{ asset('source/dist/css/mobile-angular-ui-hover.min.css') }}" />
        <link rel="stylesheet" href="{{ asset('source/dist/css/mobile-angular-ui-base.min.css') }}" />
        <link rel="stylesheet" href="{{ asset('source/dist/css/mobile-angular-ui-desktop.min.css') }}" />
        <link rel="stylesheet" href="{{ asset('source/dist/css/app.css') }}" />
        <link rel="stylesheet" href="{{ asset('source/dist/css/app2.css') }}" />
        <script src="//cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.0/angular.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.0/angular-route.min.js"></script>
        <script src="{{ asset('source/dist/js/mobile-angular-ui.min.js') }}"></script>

        <!-- Required to use $touch, $swipe, $drag and $translate services -->
        <script src="{{ asset('source/dist/js/mobile-angular-ui.gestures.min.js') }}"></script>
        <script src="{{ asset('source/dist/js/app.js') }}"></script>
    </head>

    <body ng-app="MobileAngularUiExamples"
            ng-controller="MainController"
            ui-prevent-touchmove-defaults>

        <!-- Sidebars -->
        @if(Auth::guest())
        <div ng-include="'templateApp/sidebar_no_reg.html'"
             ui-track-as-search-param='true'
             class="sidebar sidebar-left"></div>
        @else
            <div ng-include="'templateApp/sidebar_reg.html'"
                 ui-track-as-search-param='true'
                 class="sidebar sidebar-left"></div>
        @endif
        <div class="app"
             ui-swipe-right='Ui.turnOn("uiSidebarLeft")'
             ui-swipe-left='Ui.turnOff("uiSidebarLeft")'>

            <!-- Navbars -->

            <div class="navbar navbar-app">
                <div class="navbar-brand navbar-brand-center" ui-yield-to="title">
                    <a class="logoHeader" href="#/">INVESTPROFIT.NET</a>
                </div>
                <div class="btn-group pull-left">
                    <div ng-show="menuButton" ui-toggle="uiSidebarLeft" class="btn sidebar-toggle">
                        <i class="fa fa-bars fa-2x"></i>
                    </div>
                    <div ng-show="arrowBack" ng-click="back()" class="btn sidebar-toggle">
                        <i class="fa fa-arrow-left fa-2x"></i>
                    </div>
                </div>
            </div>
            {{--<div class="navbar navbar-app navbar-absolute-bottom">
                <div class="btn-group justified">
                    <a href="/" class="btn btn-navbar"><i class="fa fa-home fa-navbar fa-2x"></i></a>
                </div>
            </div>--}}
            <!-- App Body -->
            <div class="app-body" ng-class="{loading: loading}">
                <div ng-show="loading" class="app-content-loading">
                    <i class="fa fa-spinner fa-spin loading-spinner"></i>
                </div>
                <div class="app-content">
                    <ng-view></ng-view>
                </div>
            </div>
        </div><!-- ~ .app -->

        <div ui-yield-to="modals"></div>
        <script>
            $(document).ready(function(){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': "{{ csrf_token() }}"
                    },
                    beforeSend: function(){
                        // Handle the complete event
//                        alert('ajax before!');
                    },
                    complete: function(){
                        // Handle the complete event
//                        alert('ajax complete!');
                    }
                });
            });
        </script>
    </body>
</html>
